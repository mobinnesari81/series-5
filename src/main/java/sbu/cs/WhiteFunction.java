package sbu.cs;

public class WhiteFunction {
    //Variables:

    private String input1 , input2;
    private String answer;
    private int section;

    //Functions:

    public WhiteFunction(String input1 , String input2 , int section)
    {
        this.input1 = input1;
        this.input2 = input2;
        this.section = section;
        this.answer = "";
        Works();
    }

    private void Works()
    {
        switch (this.section)
        {
            case 1:
                Work1();
                break;
            case 2:
                Work2();
                break;
            case 3:
                Work3();
                break;
            case 4:
                Work4();
                break;
            case 5:
                Work5();
                break;
        }
    }

    private void Work1()
    {
        int index1 = 0 , index2 = 0;
        while(index1 < input1.length() && index2 < input2.length())
        {
            answer += (char)input1.charAt(index1);
            answer += (char)input2.charAt(index2);
            index1++;
            index2++;
        }
        while(index1 < input1.length())
        {
            answer += (char)input1.charAt(index1);
            index1++;
        }
        while(index2 < input2.length()){
            answer += (char)input2.charAt(index2);
            index2++;
        }
    }

    private void Work2()
    {
        answer += input1;
        for (int i = input2.length()-1 ; i > -1 ; i--)
        {
            answer += (char)input2.charAt(i);
        }
    }

    private void Work3()
    {
        int index1 = 0, index2 = input2.length()-1;
        while(index1 < input1.length() && index2 > -1)
        {
            answer += (char)input1.charAt(index1);
            answer += (char)input2.charAt(index2);
            index1++;
            index2--;
        }
        while(index1 < input1.length())
        {
            answer += (char)input1.charAt(index1);
            index1++;
        }
        while (index2 > -1)
        {
            answer += (char)input2.charAt(index2);
            index2--;
        }
    }

    private void Work4()
    {
        if (input1.length() % 2 == 0)
        {
            answer = input1;
        }
        else
        {
            answer = input2;
        }
    }

    private void Work5()
    {
        String code = "abcdefghijklmnopqrstuvwxyz";
        if (input1.length() <= input2.length())
        {
            for (int i = 0 ; i < input1.length() ; i++)
            {
                int index = (code.indexOf(input1.charAt(i)) + code.indexOf(input2.charAt(i))) % 26;
                answer += (char)code.charAt(index);
            }
            for (int i = input1.length() ; i < input2.length() ; i++)
            {
                answer += (char)input2.charAt(i);
            }
        }
        else
        {
            for (int i = 0 ; i < input2.length() ; i++)
            {
                int index = (code.indexOf(input2.charAt(i)) + code.indexOf(input1.charAt(i))) % 26;
                answer += (char)code.charAt(index);
            }
            for (int i = input2.length() ; i < input1.length() ; i++)
            {
                answer += (char)input1.charAt(i);
            }
        }
    }

    public String getAnswer()
    {
        return answer;
    }
}
