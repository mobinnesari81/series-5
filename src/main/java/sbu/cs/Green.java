package sbu.cs;

public class Green extends HomeRow{
    public Green(String input , int section)
    {
        super.setInput1(input);
        BlackFunction function = new BlackFunction(input , section);
        super.setOutput1(function.getAnswer());
        super.setOutput2(function.getAnswer());
        //System.out.println(super.getOutput1());
    }
}
