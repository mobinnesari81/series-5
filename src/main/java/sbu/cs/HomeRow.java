package sbu.cs;

public class  HomeRow {
    private String input1;
    private String input2;
    private String output1;
    private String output2;

    public HomeRow()
    {
        input1 = "";
        input2 = "";
        output1 = "";
        output2 = "";
    }

    public void setOutput2(String output2) {
        this.output2 = output2;
    }

    public void setOutput1(String output1) {
        this.output1 = output1;
    }

    public String getOutput2() {
        return output2;
    }

    public void setInput1(String input1) {
        this.input1 = input1;
    }

    public void setInput2(String input2) {
        this.input2 = input2;
    }


    public String getInput1() {
        return input1;
    }

    public String getInput2() {
        return input2;
    }

    public String getOutput1() {
        return output1;
    }

}
