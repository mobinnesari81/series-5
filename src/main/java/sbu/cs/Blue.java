package sbu.cs;

public class Blue extends HomeRow{
    public Blue(String input1 , String input2 , int section)
    {
        super.setInput1(input1);
        super.setInput2(input2);
        BlackFunction function1 = new BlackFunction(super.getInput1(),section);
        BlackFunction function2 = new BlackFunction(super.getInput2(),section);
        super.setOutput1(function1.getAnswer());
        super.setOutput2(function2.getAnswer());
    }
}
