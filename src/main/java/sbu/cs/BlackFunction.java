package sbu.cs;

public class BlackFunction{

    //Variables:

    private String input;
    private String answer;
    private int section;

    //Functions:

    public BlackFunction(String input , int section)
    {
        this.input = input;
        this.section = section;
        answer = "";
        Works();
    }

    public String getAnswer()
    {
        return answer;
    }

    private void Works()
    {
        switch (this.section)
        {
            case 1:
                Work1();
                break;
            case 2:
                Work2();
                break;
            case 3:
                Work3();
                break;
            case 4:
                Work4();
                break;
            case 5:
                Work5();
                break;
        }
    }

    private void Work1()
    {
        for (int i = this.input.length()-1 ; i > -1 ; i--)
        {
            this.answer += this.input.charAt(i);
            //System.out.println(answer);
        }
    }

    private void Work2()
    {
        for (int i = 0 ; i < this.input.length() ; i++)
        {
            this.answer += (char)this.input.charAt(i);
            this.answer += (char)this.input.charAt(i);
        }
    }

    private void Work3()
    {
        this.answer = this.input + this.input;
    }

    private void Work4()
    {
        this.answer += this.input.charAt(this.input.length()-1);
        for (int i = 0 ; i < this.input.length()-1 ; i++)
        {
            this.answer += (char)this.input.charAt(i);
        }
    }

    private void Work5()
    {
        String code = "abcdefghijklmnopqrstuvwxyz";
        for (int i = 0 ; i < input.length() ; i++)
        {
            //System.out.println(input.charAt(i));
            int index = code.indexOf(input.charAt(i));
            index = 25 - index;
            answer += (char)code.charAt(index);
        }
    }
}
