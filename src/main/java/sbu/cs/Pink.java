package sbu.cs;

public class Pink extends HomeRow{
    public Pink(String input1 , String input2 , int section)
    {
        super.setInput1(input1);
        super.setInput2(input2);
        WhiteFunction function = new WhiteFunction(input1 , input2 , section);
        super.setOutput1(function.getAnswer());
    }
}
