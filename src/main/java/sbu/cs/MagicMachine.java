package sbu.cs;

public class MagicMachine {
    //Variables:
    private HomeRow[][] machine;
    private int[][] numberArray;
    private String input;
    private String output;

    //Functions:
    public MagicMachine(int[][] numberArray , String input)
    {
        machine = new HomeRow[numberArray.length][numberArray.length];
        this.numberArray = numberArray;
        this.input = input;
        this.output = "";
        SetUp();
    }

    private void SetUp()
    {
        //Green Section:
        machine[0][0] = new Green(input , numberArray[0][0]);
        for (int i = 1 ; i < machine.length-1 ; i++)
        {
            machine[0][i] = new Green(machine[0][i-1].getOutput1() , numberArray[0][i]);
            System.out.println(i + " " + machine[0][i].getOutput2());
        }
        for (int i = 1 ; i < machine.length-1 ; i++)
        {
            machine[i][0] = new Green(machine[i-1][0].getOutput2() , numberArray[i][0]);
        }

        //Yellow Section:
        machine[0][machine.length-1] = new Yellow(machine[0][machine.length-2].getOutput1() , numberArray[0][numberArray.length-1]);
        machine[machine.length-1][0] = new Yellow(machine[machine.length-2][0].getOutput2() , numberArray[numberArray.length-1][0]);

        //Blue Section:
        for (int i = 1 ; i < machine.length-1 ; i++)
        {
            System.out.println(i);
            machine[i][i] = new Blue(machine[i][i-1].getOutput1() , machine[i-1][i].getOutput2() , numberArray[i][i] );
            for (int j = i+1 ; j < machine.length-1 ; j++)
            {
                machine[i][j] = new Blue(machine[i][j-1].getOutput1() , machine[i-1][j].getOutput2() , numberArray[i][j]);
            }
            for (int j = i+1 ; j < machine.length-1 ; j++)
            {
                machine[j][i] = new Blue(machine[j][i-1].getOutput1() , machine[j-1][i].getOutput2() , numberArray[j][i]);
            }
        }

        //Pink Section:
        for (int i = 1; i < machine.length-1 ; i++ )
        {
            machine[i][machine.length-1] = new Pink(machine[i][machine.length-2].getOutput1() , machine[i-1][machine.length-1].getOutput1() , numberArray[i][machine.length-1]);
        }
        for (int i = 1 ; i < machine.length-1; i++)
        {
            machine[machine.length-1][i] = new Pink(machine[machine.length-1][i-1].getOutput1() , machine[machine.length-2][i].getOutput2() , numberArray[machine.length-1][i]);
        }
        machine[machine.length-1][machine.length-1] = new Pink(machine[machine.length-1][machine.length-2].getOutput1() , machine[machine.length-2][machine.length-1].getOutput1(),numberArray[machine.length-1][machine.length-1]);
        output = machine[machine.length-1][machine.length-1].getOutput1();
    }

    public String getOutput()
    {
        return output;
    }

}
