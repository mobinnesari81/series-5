package sbu.cs;

public class Yellow extends HomeRow{
    public Yellow(String input , int section)
    {
        super.setInput1(input);
        BlackFunction function = new BlackFunction(input , section);
        super.setOutput1(function.getAnswer());
    }
}
